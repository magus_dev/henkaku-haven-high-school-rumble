This readme shows how to install and setup the sdk + unity for psvita
and it also shows how to get a vpk to be installed on the Vita.
IT MAY LOOK HARD BUT ITS EASY.
just follow along!
:)

1) Install UnitySetup64-2017.4.2f2.exe

2) Open Unity and choose License "Unity Pro" and enter the license key for PSVITA (or psvita+ps4)
License key for Unity Pro PS4 AND PSVITA :


I3-M7AD-279S-X79U-FPTT-854R



3) Close Unity and install UnitySetup-Playstation-Vita-Support-for-Editor-2017.4.2f2.exe

4) Copy the folder sdk into C:\PSVITA\ or any other folder you want. Remember the location! *1

5) Install the following exe:

-TMServer-3_60_0_2

-USBDriver-Win64-1_80_0_70253

-ScreenCapture-3_60_1_11

-NetworkManager-3_10_0_88353

-EventViewer-3_60_1_11

-ControllerCapture-3_60_1_11

-ContentManagerAssistant-3_3_0_7825 (If you have already installed CMA be sure to uninstall it first)

-BatteryMonitor-3_60_1_12

6) Add environment variables to your PC . Google this one if you don't know how. A youtube video should help.

- Add the variable SCE_PSP2_SDK_DIR and link it to your sdk path.
In my case it's :   C:\PSVITA\sdk 
If you pasted the sdk folder the same at the location in the beginning it should be the same.

- Add the variable SCE_ROOT_DIR and link it to SCE folder.
If you haven't changed the path it should be  :   C:\Program Files (x86)\SCE\

7) Reboot your pc

Now you are ready to build Unity games. Open the Unity you installed, create a new project, optionally add Psvita assets.
Then go to "Build Settings" and choose "PSVita" and press "Switch Platform".
Then you can also go to "Player Settings" and press the Vita icon and change some stuff like name and so on.




8) HOW TO BUILD
Pressing Build in the Build Settings should make a folder with lots of files.
The provided UnityTool.exe converts the output of Unity to a vpk.
It seems like using the -f flag is neccessary.
Basically open a cmd and call UnityTools exe with the correct arguments.
If you are in the same folder that contains the build stuff by unity just run this command :
"X:/Path/To/UnityTools.exe" -i "FolderNameOfOutput" -o "FolderToOutputIn/name" -p -f

(ex. "UnityTool -i Input/files/path -o output/path -p -f -u -r" )

args:

-i Input DIR

-o output DIR

-f Fix the "Unsafe homebrew" bug (that prevents the game from launching if unsafe homebrew is enabled)

-u Remove the "trial version" watermark caused by development license in the bottom right corner

-r Remove unused files from your build

-p Pack to "PC HOSTED" builds to .VPK

-d Remove input directory after packing into .vpk

Also, you can Drag n Drop a "PC HOSTED" build onto the program to run all operations on it...

Enjoy it and good development to you all. The rest is up to you! Bring us good games ! :D
