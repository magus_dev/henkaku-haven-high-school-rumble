VITA - I3-B2DJ-BSR5-HKTE-2HHH-9QR2
PS4 - I3-9HBF-CRK9-TDEA-422V-KDB5
PS4 AND VITA - I3-M7AD-279S-X79U-FPTT-854R

How to setup your Unity environment to develop vita games using Unity using 3.57 official SDK
Lately i've been into the messed setup of these tools, so i choose to write a tutorial to help the people to setup everything correctly.

Warning

This is only a tutorial, no illegal material is linked below, if you can effort an official license key, buy one and support the official way to develop games.

Requirment

- (Optional, but really suggested) Visual Studio 2017 (Community version is enough) installed

- Unity 2017.4 Release 3_ 2017.4.2

- UnitySetup-Playstation-Vita-Support-for-Editor-2017.4.2f2

- 3.570 SDK

- PS4 and PS Vita development license key

(for the links, Google is your friend, the big one isn't hard to find ;) )

-UnityTools (https://github.com/SilicaAndPina/UnityTools/archive/v1.0.zip) (Author SilicaAndPina, thank you for this tool)

1 Install Unity 2017.4 Release 3_ 2017.4.2

2 Put the license key for PS4 and PS Vita development , then install UnitySetup-Playstation-Vita-Support-for-Editor-2017.4.2f2

3 Create the folder PSVITA in the location where you want to put the SDK, i will use C:\PSVITA\ as default location for this tutorial

4 Extract "SDK-3_570_011-P03420862-P03421078-P03422528" to C:\PSVITA\

(optional) exctract every patch for the SDK-3_570 from the older to the newer, overwriting the files.

5 Extract "libsecure-2.4.6 - 3.570.071" to C:\PSVITA\

6 Extract "SNC-3_570_1" to C:\PSVITA\

7 Install "TMServer-3_60_0_2"

8 Install the following exe:

-USBDriver-Win64-1_80_0_70253

-ScreenCapture-3_60_1_11

-NetworkManager-3_10_0_88353

-EventViewer-3_60_1_11

-ControllerCapture-3_60_1_11

-ContentManagerAssistant-3_3_0_7825 (If you have already installed CMA be sure to uninstall it first)

-BatteryMonitor-3_60_1_12

Now you are almost done

You just need to set up the Envirment variables

- Add the variable SCE_PSP2_SDK_DIR and link it to your sdk path, in my case it's C:\PSVITA\sdk

- Add the variable SCE_ROOT_DIR and link it to SCE folder, if you haven't changed the path it should be: C:\Program Files (x86)\SCE\

------REBOOT YOUR PC------

CONGRATULATIONS

if you did everything correctly you should be fine, you are ready to build your first Unity game for PSVita

Use UnityTool to convert your built game to vpk using cmd (eg. "UnityTool -i Input/files/path -o output/path -p" )

args:

-i Input DIR

-o output DIR

-f Fix the "Unsafe homebrew" bug (that prevents the game from launching if unsafe homebrew is enabled)

-u Remove the "trial version" watermark caused by development license in the bottom right corner

-r Remove unused files from your build

-p Pack to "PC HOSTED" builds to .VPK

-d Remove input directory after packing into .vpk

Also, you can Drag n Drop a "PC HOSTED" build onto the program to run all operations on it...

Enjoy it and good development to you all.