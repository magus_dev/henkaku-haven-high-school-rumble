using UnityEngine;
using System.Collections;
#if UNITY_PSP2
using UnityEngine.PSVita;
#endif

public class GyroCube : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.rotation = Input.gyro.attitude;
	}
}
