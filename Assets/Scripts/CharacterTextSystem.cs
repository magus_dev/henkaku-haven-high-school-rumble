﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using System.Linq;

public class CharacterTextSystem : MonoBehaviour
{

    public static CharacterTextSystem instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.

    public const int queueAfterXChars = 38;
    public const int textIndentationChars = 4;

    public GameObject characterTextGO;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI characterText;
    public Image continueIndicatorImage;

    private bool canAdvance = false;
    private bool wantsToFinishLine = false;

    float m_TextSpeed = 1.0f;

    StoryDialog currentEvent;



    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);



    }

    string characterName = "";
    List<string> characterTextQueue = new List<string>();
    bool isExplosionText = false;

    public void DisplayCharacterText(string charactername , string text)
    {
        this.characterName = charactername;
        this.characterTextGO.gameObject.SetActive(true);

        if (text.Contains("[STARTEXPLODEUI]"))
        {
            text = text.Replace("[STARTEXPLODEUI]", "");
            if (isExplosionText)
            {

            }
            else
            {
                isExplosionText = true;
                UISystem.instance.StartExplodeUI();
            }
        }else if (text.Contains("[STOPEXPLODEUI]"))
        {
            isExplosionText = false;
            text = text.Replace("[STOPEXPLODEUI]", "");
            UISystem.instance.StopExplodeUI();
        }
        else
        {
            isExplosionText = false;
        }

        if (text.Contains("[STARTALARM]"))
        {
            text = text.Replace("[STARTALARM]", "");
            AudioManager.instance.PlayAlarm();
        }

        text = text.Replace("\r" , "");
        text = text.Replace("\n", "");

        characterTextQueue = SplitText(text);


        StartEvents();


        continueIndicatorImage.gameObject.SetActive(false);
        canAdvance = true;
        AdvanceText();
    }


    private void StartEvents()
    {
        if(currentEvent == null || currentEvent.additionalEvent == null)
        {
            return;
        }
        foreach(var ev in currentEvent.additionalEvent)
        {
            if (ev != null)
            {
                if (ev.type == EffectEvent.Type.Shake)
                {
                    ShakeEvent evt = (ShakeEvent)ev;
                    UISystem.instance.StartShakeEvent(evt);
                }
                else if (ev.type == EffectEvent.Type.SFX)
                {
                    SFXEvent evt = (SFXEvent)ev;
                    AudioManager.instance.StartSFXEvent(evt);
                }
                else if (ev.type == EffectEvent.Type.StrobeSprites)
                {
                    StrobeSpritesEvent evt = (StrobeSpritesEvent)ev;
                    UISystem.instance.StartStrobeSpritesEvent(evt);
                }
            }
        }
        
    }

    public void DisplayCharacterText(StoryDialog storyDialog)
    {
        currentEvent = storyDialog;
        DisplayCharacterText(CharacterDisplaySystem.instance.GetCharacterName(storyDialog.TALKING_CHAR_ID), storyDialog.TEXT);
    }

    List<string> SplitText(string text)
    {
        List<string> l = new List<string>();

        if(text.Length < queueAfterXChars - textIndentationChars)
        {
            l.Add(text);
        }
        else
        {


            int pointerPos = 0;
            int lastSplitPos = 0;
            bool notDone = true;

            while(notDone)
            {
                pointerPos = lastSplitPos + queueAfterXChars - textIndentationChars;
                if (pointerPos >= text.Length - 1)
                {
                    string txtToAdd = text.Substring(lastSplitPos);
                    txtToAdd = RemoveWhitespaceBeforeFirstCharacter(txtToAdd);
                    txtToAdd = IndentText(txtToAdd);
                    l.Add(txtToAdd);
                    notDone = false;
                }
                else
                {
                    int endPointerPos = pointerPos - queueAfterXChars - textIndentationChars;
                    for (int p = pointerPos; p >= endPointerPos; p--)
                    {
                        if (IsWhiteSpaceOrNewlineOrTab(text[p]))
                        {
                            string txtToAdd = text.Substring(lastSplitPos, p - lastSplitPos);
                            txtToAdd = RemoveWhitespaceBeforeFirstCharacter(txtToAdd);
                            txtToAdd = IndentText(txtToAdd);
                            l.Add(txtToAdd);
                            lastSplitPos = p;
                            p = endPointerPos - 1; // careful dont use after this line lul!
                            continue;
                        }
                    }
                }



            }

        }

        return l;
    }

    public static string IndentText(string text)
    {
        string txt = new string(' ', textIndentationChars) + text;
        return txt;
    }

    public static string RemoveWhitespaceBeforeFirstCharacter(string txt)
    {
        int firstCharStartAtPos = 0;
        for(int i = 0; i< txt.Length; i++)
        {
            if (IsWhiteSpaceOrNewlineOrTab(txt[i]))
            {

            }
            else
            {
                firstCharStartAtPos = i;
                break;
            }
        }

        txt = txt.Substring(firstCharStartAtPos);

        return txt;

    }

    public static bool IsWhiteSpaceOrNewlineOrTab(char t)
    {
        if(t == ' ' || t == '\r' || t == '\n' || t == '\t')
        {
            return true;
        }
        return false;
    }

    // return false when cant advance aka no more text left.
    public bool AdvanceText()
    {
        if (!canAdvance)
        {
            wantsToFinishLine = true;
            return true;
        }
        canAdvance = false;

        if(characterTextQueue.Count <= 0)
        {
            UISystem.instance.StopShakeEvent();
            UISystem.instance.StopStrobeSpritesEvent();
            return false;
        }else if (characterTextQueue.Count == 1) {
            StartCoroutine(AnimateText(characterTextQueue[0] , ""));
            characterTextQueue.RemoveAt(0);
            characterTextQueue.TrimExcess();
        }
        else
        {
            StartCoroutine(AnimateText(characterTextQueue[0], characterTextQueue[1]));
            characterTextQueue.RemoveAt(0);
            characterTextQueue.TrimExcess();
            characterTextQueue.RemoveAt(0);
            characterTextQueue.TrimExcess();
        }

        return true;
    }

    IEnumerator AnimateText(string txt1 , string txt2)
    {
        if(txt1 == null)
        {
            txt1 = "";
        }
        if (txt2 == null)
        {
            txt2 = "";
        }

        nameText.text = characterName;

        continueIndicatorImage.gameObject.SetActive(false);
        float iterations = 29.97f / m_TextSpeed;
        int charsPerIteration = Mathf.CeilToInt(txt1.Length / iterations);
        int nextAmount = 0;
        for (int i = 0; i < iterations; i++)
        {
            nextAmount = charsPerIteration * i;
            if (isExplosionText)
            {
                UISystem.instance.IncreaseExplodeUISpeed();
            }
            if(nextAmount >= txt1.Length)
            {
                nextAmount = txt1.Length;
                break;
            }
            characterText.text = txt1.Substring(0, nextAmount);
            yield return new WaitForSeconds(1/29.97f);
            if (wantsToFinishLine)
            {
               break;
            }
        }
        if (wantsToFinishLine)
        {
            characterText.text = txt1 + "\r\n" + txt2;
        }
        else
        {
            characterText.text = txt1;

            charsPerIteration = Mathf.CeilToInt(txt2.Length / iterations);
            for (int i = 0; i < iterations; i++)
            {
                nextAmount = charsPerIteration * i;
                if (isExplosionText)
                {
                    UISystem.instance.IncreaseExplodeUISpeed();
                }
                if (nextAmount >= txt2.Length)
                {
                    nextAmount = txt2.Length;
                    break;
                }
                characterText.text = txt1 + "\r\n" + txt2.Substring(0, nextAmount);
                yield return new WaitForSeconds(1 / 29.97f);
                if (wantsToFinishLine)
                {
                    break;
                }
            }
            characterText.text = txt1 + "\r\n" + txt2;
        }

        continueIndicatorImage.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.29f);
        canAdvance = true;
        wantsToFinishLine = false;
        yield return null;
    }

    public void Hide()
    {
        this.characterTextGO.gameObject.SetActive(false);
    }


    //Update is called every frame.
    void Update()
    {

    }
}