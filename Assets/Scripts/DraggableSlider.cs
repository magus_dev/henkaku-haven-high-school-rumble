﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableSlider : MonoBehaviour {

    bool startposSet = false;
    Vector2 startPos;

    bool finished = false;
    bool dragging = false;
    float timeUsedToSlide = 0.0f;

    public bool horizontalSlider = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (dragging)
        {
            timeUsedToSlide += Time.deltaTime;
        }
    }

    public void ResetSlider()
    {
        if (startposSet)
        {
            transform.position = startPos;
        }
        timeUsedToSlide = 0.0f;
        finished = false;
        dragging = false;
        
    }

    private void OnMouseDrag()
    {
        if (!dragging)
        {
            dragging = true;
            timeUsedToSlide = 0.0f;
        }
        if (finished)
        {
            return;
        }
        AudioManager.instance.PlaySqueakFull();
        SetGameobjectPos(Input.mousePosition, gameObject);
    }

    private void SetGameobjectPos(Vector2 pos, GameObject spr)
    {
        if(startposSet == false)
        {
            startposSet = true;
            startPos = transform.position;
        }
        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        worldScreenHeight = worldScreenWidth / (16.0f / 9.0f);

        float widthFactor = (480 / worldScreenWidth) * 2;
        float heightFactor = (272 / worldScreenHeight) * 2;

        //pos.x = (pos.x - 480) / widthFactor;

        if (horizontalSlider)
        {
            pos.x = (pos.x - 480) / widthFactor;
            pos.y = gameObject.transform.position.y;

            pos.x = Mathf.Max(Mathf.Min(pos.x, 1.8f * gameObject.transform.lossyScale.x), -1.8f * gameObject.transform.lossyScale.x);
        }
        else
        {
            pos.x = gameObject.transform.position.x;
            pos.y = (pos.y - 272) / heightFactor;

            pos.y = Mathf.Max(Mathf.Min(pos.y, 1.8f * gameObject.transform.lossyScale.y), -1.8f * gameObject.transform.lossyScale.y);
        }


        spr.transform.position = pos;
    }

    private void OnMouseUp()
    {
        dragging = false;
        if (startposSet && !finished)
        {
            transform.position = startPos;
        }
        AudioManager.instance.PlaySqueak();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag != "MoneyDragPosition" || finished)
        {
            return;
        }
        float timeCompletion = timeUsedToSlide;
        Debug.Log("Slide completed in : " + timeCompletion);
        finished = true;

        MoneyDraggingEventSystem.instance.OnSlideSuccess(timeCompletion);
    }

    internal void EnableCollider()
    {
        GetComponent<Collider2D>().enabled = true;
    }
    internal void DisableCollider()
    {
        GetComponent<Collider2D>().enabled = false;
    }
}
