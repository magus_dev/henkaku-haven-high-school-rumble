﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SystemInfoDisplay : MonoBehaviour {

    public TextMeshProUGUI tmSysInfo;

	// Use this for initialization
	void Start () {
        if(tmSysInfo == null)
        {
            tmSysInfo = GetComponent<TextMeshProUGUI>();
        }

        tmSysInfo.text = "Graphichs memory size : " + SystemInfo.graphicsMemorySize.ToString()
            + "\r\n System memory size: " + SystemInfo.systemMemorySize;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
