﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class StorySystem : MonoBehaviour
{
    public static StorySystem instance = null;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a MainSystem.
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    public StoryItem currentItem;
    public StoryItem lastItem;
    public string currentStoryId = "0";

    // Use this for initialization
    void Start()
    {
        currentStoryId = PlayerPrefs.GetString("current_story_id", "0");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public StoryItem GetNextStoryItem()
    {
        if(currentItem != null)
        {
            currentStoryId = currentItem.NEXT_ID;
        }

        TextAsset storyData = Resources.Load<TextAsset>("story/" + currentStoryId.ToString());
        if (storyData == null)
        {
            //DebugText.instance.Log("Could not find Story data !!! " + currentStoryId);
            Debug.LogError("Could not find Story data !!! " + currentStoryId);
            return null;
        }
        string content = storyData.text;
        if (content == "")
            content = System.Text.Encoding.Default.GetString(storyData.bytes);
        string[] storyDataLines = content.Split(new string[] { "\r\n", "\n", "\r" }, System.StringSplitOptions.RemoveEmptyEntries);

        long type = long.Parse(storyDataLines[0], CultureInfo.InvariantCulture);
        switch (type)
        {
            case 0:
                lastItem = currentItem;
                currentItem = MakeNextDialougeEvent(storyDataLines, currentStoryId);
                break;
            case 1:
                lastItem = currentItem;
                currentItem = MakeNextRubEvent(storyDataLines, currentStoryId);
                break;
            case 2:
                lastItem = currentItem;
                currentItem = MakeNextMoneyDragEvent(storyDataLines, currentStoryId);
                break;

        }
        return currentItem;
    }



    private StoryDialog MakeNextDialougeEvent(string[] data, string id)
    {
        StoryDialog sd = new StoryDialog();
        sd.ID = id;
        sd.type = StoryItem.StoryItemType.Dialogue;
        sd.NEXT_ID = data[1];
        sd.BACKGROUND = data[2];
        sd.TALKING_CHAR_ID = long.Parse(data[3], CultureInfo.InvariantCulture);
        sd.TEXT = data[4];
        int index = 5;
        //Debug.Log("Next dialog event : " + id + " " + sd.TEXT);
        while (index < data.Length && data[index].Contains("\t"))
        {
            string[] effecteventdata = data[index].Split(new string[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);
            sd.AddEvent(CreateEvent(effecteventdata));
            index++;
        }
        return sd;
    }
    private StoryRubEvent MakeNextRubEvent(string[] data, string id)
    {
        StoryRubEvent sre = new StoryRubEvent();
        sre.ID = id;
        sre.type = StoryItem.StoryItemType.RubEvent;
        sre.NEXT_ID = data[1];
        sre.BACKGROUND = data[2];
        sre.RUBBED_CHAR_ID = long.Parse(data[3], CultureInfo.InvariantCulture);
        List<string> rubDatas = new List<string>();
        for(int i = 4; i < data.Length; i++)
        {
            rubDatas.Add(data[i]);
        }
        sre.SetRubData(rubDatas);

        return sre;
    }
    private StoryMoneyDragEvent MakeNextMoneyDragEvent(string[] data, string id)
    {
        StoryMoneyDragEvent smde = new StoryMoneyDragEvent();
        smde.ID = id;
        smde.type = StoryItem.StoryItemType.MoneyDragEvent;
        smde.NEXT_ID = data[1];
        smde.BACKGROUND = data[2];
        int index = 3;

        while (index < data.Length && data[index].Contains("\t"))
        {
            string[] effecteventdata = data[index].Split(new string[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);
            smde.AddEvent(CreateEvent(effecteventdata) );
            index++;
        }


        smde.moneyPos = new Vector2(float.Parse(data[index], CultureInfo.InvariantCulture), float.Parse(data[index+1], CultureInfo.InvariantCulture));
        smde.destinationPos = new Vector2(float.Parse(data[index+2], CultureInfo.InvariantCulture), float.Parse(data[index+3], CultureInfo.InvariantCulture));
        smde.timeToSlide = float.Parse(data[index+4], CultureInfo.InvariantCulture);
        smde.direction = int.Parse(data[index + 5], CultureInfo.InvariantCulture);

        return smde;
    }

    private EffectEvent CreateEvent(string[] data)
    {
        if (data == null || data.Length <= 0)
        {
            return null;
        }
        long type = long.Parse(data[0], CultureInfo.InvariantCulture);
        if(type == 0)
        {
            // Shakeeffect
            float xS = float.Parse(data[1], CultureInfo.InvariantCulture);
            float yS = float.Parse(data[2], CultureInfo.InvariantCulture);
            float d = float.Parse(data[3], CultureInfo.InvariantCulture);
            return new ShakeEvent(xS,yS,d);
        }else if(type == 12)
        {
            string audioClip = data[1];
            return new SFXEvent(audioClip);
        }else if(type == 25)
        {
            float duration = float.Parse(data[1], CultureInfo.InvariantCulture);
            List<string> spriteNames = new List<string>();
            for(int i = 2; i < data.Length; i++)
            {
                spriteNames.Add(data[i]);
            }
            return new StrobeSpritesEvent(duration, spriteNames);
        }

        return null;
    }


}
