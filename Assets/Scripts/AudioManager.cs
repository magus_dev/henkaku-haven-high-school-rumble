﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager instance;

    AudioSource sfxSource;
    AudioSource bgmSource;
    AudioSource musicSource;

    public AudioClip musicBG;
    public AudioClip musicRubbing;

    public AudioClip squeakClip;
    public AudioClip aaaScreamClip;
    public AudioClip alarmClip;

    const float BGM_VOLUME_DEFAULT = 0.125f;
    const float BGM_VOLUME_QUIET = 0.005f;

    // Use this for initialization
    void Awake () {
        instance = this;
        sfxSource = FindObjectOfType<AudioSource>();
        musicSource = sfxSource.gameObject.AddComponent<AudioSource>();
        bgmSource = sfxSource.gameObject.AddComponent<AudioSource>();

        sfxSource.loop = false;
        sfxSource.volume = 0.1f;
        sfxSource.playOnAwake = false;
        musicSource.loop = true;
        musicSource.volume = 0.1f;
        musicSource.playOnAwake = false;
        bgmSource.loop = true;
        bgmSource.volume = BGM_VOLUME_DEFAULT;
        bgmSource.playOnAwake = false;

        PlayBGM();
    }

    private void PlayBGM()
    {
        if(bgmSource == null || musicBG == null)
        {
            return;
        }
        else
        {
            bgmSource.clip = musicBG;
            bgmSource.Play();
        }
    }

    private void PlayMusic(AudioClip clip)
    {
        if (clip == null || musicSource == null)
        {
            return;
        }
        else
        {
            if(musicSource.clip == clip && musicSource.isPlaying)
            {
                return;
            }

            musicSource.loop = true;
            musicSource.clip = clip;
            musicSource.Play();
            bgmSource.volume = BGM_VOLUME_QUIET;
        }
    }


    private void StopMusic()
    {
        if (musicSource == null)
        {
            return;
        }
        // We could use Pause here so it doesn't start from 0 always if you want to continue to play the song
        //musicSource.Pause();
        musicSource.Stop();
        bgmSource.volume = BGM_VOLUME_DEFAULT;
    }

    private void PlaySfx(AudioClip clip, bool oneShot, bool quietBGM)
    {
        if (clip == null || sfxSource == null)
        {
            return;
        }
        else
        {
            

            if (oneShot)
            {
                sfxSource.PlayOneShot(clip);
            }
            else if(!sfxSource.isPlaying)
            {
                sfxSource.clip = (clip);
                sfxSource.Play();
            }

        }
    }

    private void ResetBgmVolume()
    {
        bgmSource.volume = BGM_VOLUME_DEFAULT;
    }

    public void PlaySqueakFull()
    {
        PlaySfx(squeakClip, false, false);
    }

    public void PlaySqueak()
    {
        PlaySfx(squeakClip, true, false);
    }

    public void PlayRubbingMusic()
    {
        EndAllSfx();
        PlayMusic(musicRubbing);
        
    }

    private void EndAllSfx()
    {
        if(sfxSource == null)
        {
            return;
        }
        sfxSource.Stop();
    }

    public void PlayAlarm()
    {
        PlayMusic(alarmClip);
    }

    public void EndRubbingMusic()
    {
        StopMusic();
    }

	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartSFXEvent(SFXEvent evt)
    {
        AudioClip a = Resources.Load<AudioClip>("audio/sfx/" + evt.audioClip);
        PlaySfx(a, true, true);
    }
}
