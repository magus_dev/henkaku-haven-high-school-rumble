﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeEvent : EffectEvent {

    public float xShakeAmount = 0.05f;
    public float yShakeAmount = 0.05f;
    public float duration = 0.0f; // less than and equal 0 means infinite

    public ShakeEvent()
    {
        type = EffectEvent.Type.Shake;
    }

    public ShakeEvent(float xShakeAmount, float yShakeAmount, float duration)
    {
        this.type = EffectEvent.Type.Shake;
        this.xShakeAmount = xShakeAmount;
        this.yShakeAmount = yShakeAmount;
        this.duration = duration;
    }
}
