﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXEvent : EffectEvent {

    public string audioClip;

	public SFXEvent()
    {
        this.type = Type.SFX;
    }
    public SFXEvent(string audioClip)
    {
        this.type = Type.SFX;
        this.audioClip = audioClip;
    }

}
