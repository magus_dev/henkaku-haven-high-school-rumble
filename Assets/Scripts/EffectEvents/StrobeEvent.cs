﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrobeSpritesEvent : EffectEvent {

    public float duration = 0.75f;
    public List<Sprite> sprites;

    public int currentIndex = -1;

    public override void Destroy()
    {
        base.Destroy();
        sprites.Clear();
    }

    public StrobeSpritesEvent()
    {
        this.type = Type.StrobeSprites;
        this.sprites = new List<Sprite>();
    }

    public StrobeSpritesEvent(float duration , List<string> spriteNames)
    {
        this.type = Type.StrobeSprites;
        this.duration = duration;
        this.sprites = new List<Sprite>();
        foreach (var sprName in spriteNames)
        {
            if (string.IsNullOrEmpty(sprName))
            {
                continue;
            }
            Sprite s = Resources.Load<Sprite>("backgrounds/" + sprName);
            if(s != null)
            {
                this.sprites.Add(s);
            }
        }
    }

    public int GetNextIndex()
    {
        if(currentIndex < 0)
        {
            currentIndex = 0;
        }
        else
        {
            currentIndex++;
        }

        if(currentIndex >= sprites.Count)
        {
            currentIndex = 0;
        }

        return currentIndex;
    }

    public Sprite GetNextSprite()
    {
        if(sprites == null || sprites.Count <= 0)
        {
            return null;
        }
        if(currentIndex == -1)
        {
            currentIndex = 0;
            return sprites[currentIndex];
        }
        currentIndex++;

        if (currentIndex >= sprites.Count)
        {
            currentIndex = 0;
        }

        if(sprites[currentIndex] == null)
        {
            return GetNextSprite();
        }

        return sprites[currentIndex];

    }
}
