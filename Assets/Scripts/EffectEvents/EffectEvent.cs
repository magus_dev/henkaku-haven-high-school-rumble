﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectEvent  {
    public enum Type
    {
        Shake,
        SFX,
        StrobeSprites,
    }
    public Type type;

    public virtual void Destroy()
    {

    }

}
