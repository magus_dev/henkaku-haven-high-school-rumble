﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class OLDStorySystem : MonoBehaviour {
    public static OLDStorySystem instance = null;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a MainSystem.
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);



    }

    string[] storyDataLines;
    public StoryItem currentItem;
    public StoryItem lastItem;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public void LoadStory()
    {
        TextAsset storyData = Resources.Load<TextAsset>("story_data");
        if(storyData == null)
        {
            DebugText.instance.Log("Could not find Story data !!!");
            Debug.LogError("Could not find Story data !!!");
            return;
        }
        string content = storyData.text;
        if (content == "")
            content = System.Text.Encoding.Default.GetString(storyData.bytes);

        DebugText.instance.Log("Loaded story data.");
        Debug.Log("Loaded story data.");

        storyDataLines = content.Split(new string[] { "\r\n", "\n", "\r" }, System.StringSplitOptions.RemoveEmptyEntries );

        Debug.Log("Data lines : " + (storyDataLines.Length - 1));

    }

    public StoryItem GetNextStoryItem()
    {
        string next_id = "0";
        if (currentItem == null)
        {
            next_id = "0";
        }
        else
        {
            next_id = currentItem.NEXT_ID;
        }
        next_id = next_id + 1; // Because the string array stores 1 additionally at the beginning.
        lastItem = currentItem;
        if(currentItem != null)
        {
            Debug.Log("Next story id : " + next_id + " c id : " + currentItem.ID + " c nextid : " + currentItem.NEXT_ID);
        }
        currentItem = GetNextStoryItem(next_id);

        return currentItem;
    }

    private StoryItem GetNextStoryItem(string next_id)
    {
        // OUTDATED :
        string storyDataLine = "0";//storyDataLines[next_id];
        string[] datas = storyDataLine.Split(new string[] { "\t" }, System.StringSplitOptions.None);

        if(datas.Length < 3)
        {
            return null;
        }
        StoryItem.StoryItemType storyItemType = StoryItem.StoryItemType.Dialogue;
        long type = long.Parse(datas[2], CultureInfo.InvariantCulture);
        switch (type)
        {
            case 0:
                storyItemType = StoryItem.StoryItemType.Dialogue;
                break;
            case 1:
                storyItemType = StoryItem.StoryItemType.RubEvent;
                break;
            case 2:
                storyItemType = StoryItem.StoryItemType.MoneyDragEvent;
                break;
            default:
                Debug.LogError("Unknown story item type !");
                break;
        }


        if (storyItemType == StoryItem.StoryItemType.Dialogue)
        {
            StoryDialog d = new StoryDialog();
            d.ID = datas[0];
            d.NEXT_ID = datas[1];
            d.type = storyItemType;
            d.TEXT = datas[3];
            d.TALKING_CHAR_ID = long.Parse(datas[4], CultureInfo.InvariantCulture);
            return d;
        }else if(storyItemType == StoryItem.StoryItemType.RubEvent)
        {
            StoryRubEvent r = new StoryRubEvent();
            r.ID = datas[0];
            r.NEXT_ID = datas[1];
            r.type = storyItemType;
            r.RUBBED_CHAR_ID = long.Parse(datas[3], CultureInfo.InvariantCulture);
            r.SetRubData( datas[4] );
            return r;
        }
        else if (storyItemType == StoryItem.StoryItemType.MoneyDragEvent)
        {
            StoryMoneyDragEvent m = new StoryMoneyDragEvent();
            m.ID = datas[0];
            m.NEXT_ID = datas[1];
            m.type = storyItemType;
            m.moneyPos = new Vector2(float.Parse(datas[3], CultureInfo.InvariantCulture), float.Parse(datas[4], CultureInfo.InvariantCulture));
            m.destinationPos = new Vector2(float.Parse(datas[5], CultureInfo.InvariantCulture), float.Parse(datas[6], CultureInfo.InvariantCulture));
            return m;
        }

        return null;
    }
    

}
