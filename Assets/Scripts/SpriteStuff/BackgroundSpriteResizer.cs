﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpriteResizer : MonoBehaviour {

    public static Vector3 spriteScale = new Vector3(1, 1, 1);

    // Use this for initialization
    void Start()
    {
        Resize();

    }

    void Resize()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr == null) return;

        Transform p = transform.parent;

        p.localScale = new Vector3(1, 1, 1);

        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;


        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        worldScreenHeight = worldScreenWidth / (16.0f / 9.0f);

        Vector3 xWidth = p.localScale;
        xWidth.x = worldScreenWidth / width;
        p.localScale = xWidth;
        //transform.localScale.x = worldScreenWidth / width;
        Vector3 yHeight = p.localScale;
        yHeight.y = worldScreenHeight / height;
        p.localScale = yHeight;
        spriteScale = yHeight;
        //transform.localScale.y = worldScreenHeight / height;

    }
}
