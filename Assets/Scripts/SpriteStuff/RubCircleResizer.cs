﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubCircleResizer : MonoBehaviour {

    public Collider2D theCollider;

    // Use this for initialization
    void Start()
    {
        if(theCollider == null)
        {
            theCollider = transform.parent.GetComponentInChildren<Collider2D>();
        }
        Resize();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void Resize()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr == null) return;
        
        Vector3 newScale = transform.localScale;
        newScale.x *= BackgroundSpriteResizer.spriteScale.x;
        newScale.y *= BackgroundSpriteResizer.spriteScale.y;
        newScale.z *= BackgroundSpriteResizer.spriteScale.z;
        transform.localScale = newScale;

        theCollider.transform.localScale = transform.localScale * 1.5f;
    }
}
