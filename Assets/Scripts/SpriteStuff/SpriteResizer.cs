﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteResizer : MonoBehaviour {

    // Use this for initialization
    void Start () {
        Resize();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Resize()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr == null) return;
        Vector3 newScale = transform.localScale;
        newScale.x *= BackgroundSpriteResizer.spriteScale.x;
        newScale.y *= BackgroundSpriteResizer.spriteScale.y;
        newScale.z *= BackgroundSpriteResizer.spriteScale.z;
        transform.localScale = newScale;

    }
}
