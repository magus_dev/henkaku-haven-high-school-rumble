﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyDraggable : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    bool mouseWasDragged = false;

    bool winCollider = false;

    private void OnMouseDrag()
    {
        mouseWasDragged = true;
        MoneyDraggingEventSystem.instance.SetMoneyPosOnMouse();
    }

    private void OnMouseUp()
    {
        if (mouseWasDragged)
        {
            MoneyDraggingEventSystem.instance.OnThrow(EnableWinCollider);
        }
        mouseWasDragged = false;
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (winCollider == false)
        {
            return;
        }
        if (collision.tag != "MoneyDragPosition")
        {
            return;
        }
        MoneyDraggingEventSystem.instance.OnDragSuccess();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    }


    private void EnableWinCollider()
    {
        winCollider = true;
    }
    public void DisableWinCollider()
    {
        winCollider = false;
    }
}
