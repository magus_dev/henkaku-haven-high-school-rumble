﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDisplaySystem : MonoBehaviour
{
    public static CharacterDisplaySystem instance = null;
    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);



    }

    public SpriteRenderer background;

    List<string> charactersDisplay;

    Dictionary<long, string> characterNames;

    // Use this for initialization
    void Start () {
        LoadCharacters();

    }

    private void LoadCharacters()
    {
        characterNames = new Dictionary<long, string>();
        TextAsset characterData = Resources.Load<TextAsset>("characters");
        if (characterData == null)
        {
            return;
        }
        string content = characterData.text;
        if (content == "")
            content = System.Text.Encoding.Default.GetString(characterData.bytes);

        string[] cDatas = content.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
        characterNames.Add(-1, "Narrator");
        for (int i = 0; i < cDatas.Length; i++)
        {
            characterNames.Add(i, cDatas[i]);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void SetBackground(string bgname)
    {
        if (string.IsNullOrEmpty(bgname))
        {
            return;
        }
        Sprite bg = Resources.Load<Sprite>("backgrounds/" + bgname);
        if (bg == null)
        {
            return;
        }
        if(background == null)
        {
            return;
        }
        background.sprite = bg;
    }

    public void DisplayCharacter(long characterId, Vector2 position)
    {

    }

    public string GetCharacterName(long TALKING_CHAR_ID)
    {
        if (characterNames.ContainsKey(TALKING_CHAR_ID))
        {
            return characterNames[TALKING_CHAR_ID];
        }
        else
        {
            return "";
        }
    }
}
