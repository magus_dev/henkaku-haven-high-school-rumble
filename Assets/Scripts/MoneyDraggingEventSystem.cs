﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyDraggingEventSystem : MonoBehaviour {

    public static MoneyDraggingEventSystem instance;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    

    // Use this for initialization
    void Start () {
        moneyDragRootUI.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject moneyDragRootUI;
    public TextMeshProUGUI moneyDragText;
    public GameObject moneySprite;
    public GameObject dragPositionSprite;
    public GameObject ghostIndicatorsRootGO;
    public GameObject handGO;
    Animator handAnimator;
    public Animator borderFlashAnimator;
    public GameObject slider;
    public GameObject sliderUpDown;
    public GameObject sliderRightToLeft;
    public GameObject sliderLeftToRight;
    DraggableSlider draggableSlider;

    StoryMoneyDragEvent currentEvent;

    Action onSuccess;
    Action onFailure;


    public void StartMoneyDraggingEvent(StoryMoneyDragEvent md, Action s, Action f)
    {
        currentEvent = md;
        onSuccess = s;
        onFailure = f;



        moneyDragRootUI.SetActive(true);
        handAnimator = handGO.GetComponent<Animator>();
        handAnimator.SetTrigger("reset");
        handAnimator.ResetTrigger("reset");

        SetSliders();

        draggableSlider.ResetSlider();

        MoneyDraggable d = moneySprite.GetComponent<MoneyDraggable>();
        d.DisableWinCollider();
        moneySprite.transform.localScale = new Vector3(1, 1, 1);

        Animator ghostAnimator = ghostIndicatorsRootGO.GetComponent<Animator>();
        ghostAnimator.speed = 1.0f / currentEvent.timeToSlide;

        SetMoneyPos(currentEvent.moneyPos);
        SetDestinationPos(currentEvent.destinationPos);

        StartEvents();

    }


    private void StartEvents()
    {
        if (currentEvent == null || currentEvent.additionalEvent == null)
        {
            return;
        }
        foreach (var ev in currentEvent.additionalEvent)
        {
            if (ev != null)
            {
                if (ev.type == EffectEvent.Type.Shake)
                {
                    ShakeEvent evt = (ShakeEvent)ev;
                    UISystem.instance.StartShakeEvent(evt);
                }
                else if (ev.type == EffectEvent.Type.SFX)
                {
                    SFXEvent evt = (SFXEvent)ev;
                    AudioManager.instance.StartSFXEvent(evt);
                }
                else if (ev.type == EffectEvent.Type.StrobeSprites)
                {
                    StrobeSpritesEvent evt = (StrobeSpritesEvent)ev;
                    UISystem.instance.StartStrobeSpritesEvent(evt);
                }
            }
        }

    }

    private void SetSliders()
    {

        if (currentEvent.direction == 0)
        {
            handAnimator.SetTrigger("vibrate_bottom");
            slider.SetActive(true);
            sliderRightToLeft.SetActive(false);
            sliderLeftToRight.SetActive(false);
            sliderUpDown.SetActive(false);
            draggableSlider = slider.GetComponentInChildren<DraggableSlider>();
        }
        else if (currentEvent.direction == 1)
        {
            handAnimator.SetTrigger("vibrate_top");
            sliderUpDown.SetActive(true);
            slider.SetActive(false);
            sliderRightToLeft.SetActive(false);
            sliderLeftToRight.SetActive(false);
            draggableSlider = sliderUpDown.GetComponentInChildren<DraggableSlider>();
        }
        else if (currentEvent.direction == 2)
        {
            handAnimator.SetTrigger("vibrate_right");
            sliderRightToLeft.SetActive(true);
            sliderUpDown.SetActive(false);
            slider.SetActive(false);
            sliderLeftToRight.SetActive(false);
            draggableSlider = sliderRightToLeft.GetComponentInChildren<DraggableSlider>();
        }
        else 
        {
            handAnimator.SetTrigger("vibrate_left");
            sliderLeftToRight.SetActive(true);
            sliderUpDown.SetActive(false);
            slider.SetActive(false);
            sliderRightToLeft.SetActive(false);
            draggableSlider = sliderLeftToRight.GetComponentInChildren<DraggableSlider>();
        }
    }
    
    public void OnDragSuccess()
    {
        moneyDragRootUI.SetActive(false);
        onSuccess();
    }

    IEnumerator DragFailCo()
    {
        moneyDragRootUI.SetActive(false);
        SetSliders();
        yield return new WaitForSeconds(0.2f);
        
        draggableSlider.DisableCollider();
        draggableSlider.ResetSlider();
        yield return new WaitForSeconds(0.2f);
        draggableSlider.EnableCollider();
        onFailure();
    }
    public void OnDragFailure()
    {
        StartCoroutine(DragFailCo());
        handAnimator.SetTrigger("reset");

    }

    IEnumerator throwCo(Action enableColl)
    {
        float scale = moneySprite.transform.localScale.x;
        for(int i = 0; i < 16; i++)
        {
            scale -= 0.025f;
            moneySprite.transform.localScale = new Vector3(scale, scale, scale);
            yield return new WaitForSeconds(0.01f);
        }

        enableColl();
    }

    IEnumerator MoveHandWait(float timeCompletion)
    {
        yield return new WaitForSeconds(1.0f);
        handAnimator.SetTrigger("move");
        yield return new WaitForSeconds(1.0f);

        slider.SetActive(false);
        sliderUpDown.SetActive(false);

        if (timeCompletion <= currentEvent.timeToSlide)
        {
            // show win animation
            borderFlashAnimator.SetTrigger("flash");
            yield return new WaitForSeconds(2.0f);
            borderFlashAnimator.SetTrigger("off");
            UISystem.instance.StopShakeEvent();
            OnDragSuccess();
        }
        else
        {
            // show loss animation
            borderFlashAnimator.SetTrigger("flash_fail");
            yield return new WaitForSeconds(2.0f);
            borderFlashAnimator.SetTrigger("off");
            UISystem.instance.StopShakeEvent();
            OnDragFailure();
        }
    }

    public void OnSlideSuccess(float timeCompletion)
    {
        StartCoroutine(MoveHandWait(timeCompletion));

        
    }

    public void OnThrow(Action enableCollider)
    {
        StartCoroutine(throwCo(enableCollider));
    }

    public void SetMoneyPosOnMouse()
    {
        SetMoneyPos(Input.mousePosition);
    }

    private void SetGameobjectPos(Vector2 pos, GameObject spr)
    {
        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        worldScreenHeight = worldScreenWidth / (16.0f / 9.0f);

        float widthFactor = (480 / worldScreenWidth) * 2;
        float heightFactor = (272 / worldScreenHeight) * 2;

        pos.x = (pos.x - 480) / widthFactor;
        pos.y = (pos.y - 272) / heightFactor;

        spr.transform.position = pos;
    }

    private void SetMoneyPos(Vector2 newPos)
    {
        SetGameobjectPos(newPos, moneySprite);
    }

    private void SetDestinationPos(Vector2 destinationPos)
    {
        SetGameobjectPos(destinationPos, dragPositionSprite);
    }

}
