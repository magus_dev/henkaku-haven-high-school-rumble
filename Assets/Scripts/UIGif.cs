﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGif : MonoBehaviour {

    public bool autoplay = false;
    public bool hideonstop = false;
    public bool playonce = false;
    public bool animationPingPong = false;
    public bool animationReverse = false;
    public Sprite[] gifSprites;
    public float timePerFrame = 1.0f / 30.0f;

    private float timePassedSinceLastFrameChange = 0.0f;
    private int currentIndex = 0;
    private int direction = 1;
    private UnityEngine.UI.Image image;
    private bool playing = false;
    private bool isstopping = false;
    private bool cantplay = false;
    

    public void Play()
    {
        if (image != null)
        {
            image.gameObject.SetActive(true);
        }
        playing = true;
        playonce = true;
    }

    public void PlayInfinite()
    {
        if(image != null)
        {
            image.gameObject.SetActive(true);
        }
        playing = true;
        playonce = false;
    }

    public void StopImmediately()
    {
        playing = false;
        autoplay = false;
        isstopping = true;
        currentIndex = 0;
        if (gifSprites != null)
        {
            if (currentIndex < gifSprites.Length)
            {
                if (gifSprites[currentIndex] != null)
                {
                    image.sprite = gifSprites[currentIndex];
                }
            }
        }
        isstopping = false;
        if (hideonstop)
        {
            image.gameObject.SetActive(false);
        }
    }

    // Use this for initialization
    void Start () {
        image = GetComponent<UnityEngine.UI.Image>();

        if(image == null)
        {
            cantplay = true;
            Debug.LogError("UIGif : No Image component attached on " + gameObject.name);
        }
        if (gifSprites == null)
        {
            cantplay = true;
            Debug.LogError("UIGif : No sprites attached on " + gameObject.name);
        }
        if (gifSprites.Length <= 0)
        {
            cantplay = true;
            Debug.LogError("UIGif : No sprites attached on " + gameObject.name);
        }

        if (animationReverse)
        {
            direction = -1;
            currentIndex = gifSprites.Length - 1;
        }
        else
        {
            direction = 1;
            currentIndex = 0;
        }

    }
	
	// Update is called once per frame
	void Update () {

        if (autoplay)
        {
            autoplay = false;
            playing = true;
        }

        if (cantplay)
        {
            playing = false;
        }

        if( playing)
        {
            Animate();
        }


	}

    private void Animate()
    {
        timePassedSinceLastFrameChange += Time.deltaTime;
        if (timePassedSinceLastFrameChange > timePerFrame)
        {
            timePassedSinceLastFrameChange = 0.0f;

            currentIndex += direction;
            if (currentIndex >= gifSprites.Length)
            {
                if (animationPingPong)
                {
                    direction = -1;
                    currentIndex += direction;
                }
                else
                {
                    currentIndex = 0;
                    if (playonce)
                    {
                        playonce = false;
                        playing = false;
                    }
                }
            }
            else if (currentIndex < 0)
            {
                if (animationPingPong)
                {
                    direction = 1;
                    currentIndex += direction;
                    if (playonce)
                    {
                        playonce = false;
                        playing = false;
                    }
                }
                else
                {
                    if (playonce)
                    {
                        playonce = false;
                        playing = false;
                        currentIndex = 0;
                    }
                    else
                    {
                        currentIndex = gifSprites.Length - 1;
                    }
                }
            }

            if (isstopping)
            {
                currentIndex = 0;
                isstopping = false;
            }
            if(gifSprites != null)
            {
                if (gifSprites[currentIndex] != null)
                {
                    image.sprite = gifSprites[currentIndex];
                }
            }
            
        }
    }

}
