﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RubbingEventSystem : MonoBehaviour {


    public static RubbingEventSystem instance;
    // Use this for initialization
    void Awake () {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    public GameObject rubCircleSpriteGO;
    public GameObject rubCircleGO;

    public GameObject rubbingUIRoot;

    public TextMeshProUGUI rubsLeftText;
    public TextMeshProUGUI timeLeftText;

    public Animator borderFlashAnimator;

    StoryRubEvent currentRubEvent;

    bool scaleSet = false;
    Vector3 scaleNormal;

    public void OnRubbed()
    {
        //Debug.Log("RUBBED");
        if (timerRunning)
        {
            timeLeftMiliseconds += 100;
            AudioManager.instance.PlaySqueak();
            rubs_amount++;
        }
    }

    float timeLeftMiliseconds = 0.0f;
    bool timerRunning = false;
    Action onSuccessAction;
    Action onFailureAction;

    long rubs_amount;
    long needed_rubs;

    public void StartRubbingEvent(StoryRubEvent storyRubEvent, Action onSuccess, Action onFailure)
    {
        if (!scaleSet)
        {
            scaleNormal = rubCircleSpriteGO.transform.localScale;
            scaleSet = true;
        }
        RubCircleCollision.instance.pointerIsOver = false;
        AudioManager.instance.PlayRubbingMusic();
        currentRubEvent = storyRubEvent;
        this.onSuccessAction = onSuccess;
        this.onFailureAction = onFailure;

        currentRubEvent.Reset();

        BeginNextPhase();
    }

    private bool BeginNextPhase()
    {
        StoryRubEvent.RubEventData red = currentRubEvent.GetNextRubData();
        if (red == null)
        {
            return false;
        }
        this.rubs_amount = 0;
        this.needed_rubs = red.amount_of_rubs;
        this.timeLeftMiliseconds = red.time_in_miliseconds;
        timerRunning = true;
        rubbingUIRoot.SetActive(true);
        rubsLeftText.gameObject.SetActive(true);
        timeLeftText.gameObject.SetActive(true);
        rubCircleGO.SetActive(true);

        CircleSetPosition(red);

        SetUI();
        return true;
    }

    IEnumerator Flashing()
    {
        borderFlashAnimator.SetTrigger("flash");
        yield return new WaitForSeconds(0.6f);
        borderFlashAnimator.SetTrigger("off");
    }

    IEnumerator FlashingFailure()
    {
        borderFlashAnimator.SetTrigger("flash_fail");
        yield return new WaitForSeconds(0.6f);
        borderFlashAnimator.SetTrigger("off");
    }

    private void CircleSetPosition(StoryRubEvent.RubEventData red)
    {
        Vector3 pos = rubCircleGO.transform.position;

        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        worldScreenHeight = worldScreenWidth / (16.0f / 9.0f);

        float widthFactor = (480 / worldScreenWidth) *2  ;
        float heightFactor = (272 / worldScreenHeight) * 2;

        pos.x = (red.xPos - 480) / widthFactor;
        pos.y = (red.yPos - 272) / heightFactor;

        rubCircleGO.transform.position = pos;
    }

    public void Hide()
    {
        timerRunning = false;
        rubbingUIRoot.SetActive(false);
        rubsLeftText.gameObject.SetActive(false);
        timeLeftText.gameObject.SetActive(false);
        rubCircleGO.SetActive(false);
    }

    private void SetUI()
    {
        rubsLeftText.text = (needed_rubs - this.rubs_amount).ToString();
        timeLeftText.text = (this.timeLeftMiliseconds/1000).ToString("0.0");
    }
    

    void Start()
    {
        rubbingUIRoot.SetActive(false);
        rubsLeftText.gameObject.SetActive(false);
        timeLeftText.gameObject.SetActive(false);
        rubCircleGO.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        if (timerRunning)
        {
            SetUI();
            InputSystem.instance.ReadTouches();
            timeLeftMiliseconds -= Time.deltaTime * 1000;

            float percentTimeLeft = timeLeftMiliseconds / currentRubEvent.GetCurrentRubData().time_in_miliseconds;
            rubCircleSpriteGO.transform.localScale = scaleNormal * percentTimeLeft;

            if (rubs_amount >= needed_rubs)
            {
                if (BeginNextPhase()) {
                    // Next phase begins now !
                    StartCoroutine(Flashing());
                }
                else
                {
                    timerRunning = false;
                    AudioManager.instance.EndRubbingMusic();
                    onSuccessAction();
                }
            }else if(timeLeftMiliseconds <= 0)
            {
                timerRunning = false;
                //AudioManager.instance.EndRubbingMusic();
                StartCoroutine(FlashingFailure());
                onFailureAction();
            }
        }
    }
}
