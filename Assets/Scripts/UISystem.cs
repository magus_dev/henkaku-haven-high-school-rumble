﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISystem : MonoBehaviour {
    public static UISystem instance;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    public GameObject rootUI;
    public GameObject backgroundSpriteGO;
    SpriteRenderer backgroundSpriteRenderer;
    public Animator borderFlashAnimator;

    float explodeRemainingTime = 0;
    private float SPEED_INCREASE = 0.3f;
    float explodeSpeed = 1.0f;
    bool explodeUI = false;


    bool shakeEvent = false;
    float shakeSpeed = 1.0f;
    float shakeRemainingTime = 0;
    float xShakeAmount = 0.02f;
    float yShakeAmount = 0.02f;


    // Use this for initialization
    void Start () {
        backgroundSpriteRenderer = backgroundSpriteGO.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {


        UpdateExplodeUI();
        UpdateShakeEvent();
        UpdateStrobeSpritesEvent();
    }

    private void UpdateShakeEvent()
    {
        if (!shakeEvent)
        {
            return;
        }
        shakeRemainingTime -= Time.deltaTime;
        if (shakeRemainingTime <= 0)
        {
            StopShakeEvent();
            shakeEvent = false;
            return;
        }
        Vector3 pos = backgroundSpriteGO.transform.position;
        pos.x += UnityEngine.Random.Range(-0.02f * shakeSpeed, 0.02f * shakeSpeed);
        pos.y += UnityEngine.Random.Range(-0.02f * shakeSpeed, 0.02f * shakeSpeed);
        pos.x = Mathf.Min(Mathf.Max(-xShakeAmount, pos.x), xShakeAmount);
        pos.y = Mathf.Min(Mathf.Max(-yShakeAmount, pos.y), yShakeAmount);
        backgroundSpriteGO.transform.position = pos;
    }

    private void UpdateExplodeUI()
    {
        if (!explodeUI)
        {
            return;
        }
        explodeRemainingTime -= Time.deltaTime;
        if(explodeRemainingTime <= 0)
        {
            StopExplodeUI();
            explodeUI = false;
            return;
        }
        //Vector3 rot = backgroundSprite.transform.rotation.eulerAngles;
        //rot.z += UnityEngine.Random.Range(-0.05f * explodeSpeed, 0.05f * explodeSpeed) ;
        //rot.z = Mathf.Sign(rot.z) * Mathf.Min(3.0f, Mathf.Abs(rot.z));
        //backgroundSprite.transform.rotation = Quaternion.Euler(rot);
        //
        //Vector3 scale = backgroundSprite.transform.localScale;
        //scale.x += UnityEngine.Random.Range(-0.005f * explodeSpeed, 0.005f * explodeSpeed);
        //scale.y += UnityEngine.Random.Range(-0.005f * explodeSpeed, 0.005f * explodeSpeed);
        //scale.y = Mathf.Min(Mathf.Max(0.9f, Mathf.Abs(scale.y)), 1.4f);
        //scale.x = Mathf.Min(Mathf.Max(0.9f, Mathf.Abs(scale.x)), 1.4f);
        //backgroundSprite.transform.localScale = scale;

        Vector3 pos = backgroundSpriteGO.transform.position;
        pos.x += UnityEngine.Random.Range(-0.02f * explodeSpeed, 0.02f * explodeSpeed);
        pos.y += UnityEngine.Random.Range(-0.02f * explodeSpeed, 0.02f * explodeSpeed);
        pos.x = Mathf.Min(Mathf.Max(-xShakeAmount, pos.x), xShakeAmount);
        pos.y = Mathf.Min(Mathf.Max(-yShakeAmount, pos.y), yShakeAmount);
        backgroundSpriteGO.transform.position = pos;

    }

    StrobeSpritesEvent strobeSpriteEvent;
    float timeToNextSprite = 1.0f;
    bool strobeSpriteEventOn = false;
    List<SpriteRenderer> strobeSprRenderers;

    public void StartStrobeSpritesEvent(StrobeSpritesEvent evt)
    {
        if(evt == null || evt.sprites == null || evt.sprites.Count <= 0)
        {
            return;
        }
        strobeSpriteEvent = evt;
        strobeSprRenderers = new List<SpriteRenderer>();
        backgroundSpriteRenderer.sprite = evt.sprites[0];
        for (int i = 1; i < evt.sprites.Count; i++)
        {
            GameObject go = new GameObject("strobe_" + i);
            go.transform.SetParent(backgroundSpriteGO.transform.parent);
            go.transform.localScale = new Vector3(1, 1, 1);
            SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
            sr.sprite = evt.sprites[i];
            strobeSprRenderers.Add(sr);
            sr.enabled = false;
        }
        timeToNextSprite = strobeSpriteEvent.duration;
        strobeSpriteEventOn = true;
    }

    public void StopStrobeSpritesEvent()
    {
        backgroundSpriteRenderer.enabled = true;
        if (strobeSprRenderers != null)
        {
            for(int i = 0; i < strobeSprRenderers.Count; i++)
            {
                if(strobeSprRenderers[i] != null)
                {
                    if(strobeSprRenderers[i].gameObject != null)
                    {
                        Destroy(strobeSprRenderers[i].gameObject);
                    }
                }
            }
            strobeSprRenderers.Clear();
        }
        strobeSpriteEventOn = false;
        if (strobeSpriteEvent != null)
        {
            strobeSpriteEvent.Destroy();
            strobeSpriteEvent = null;
        }
    }

    private void UpdateStrobeSpritesEvent()
    {
        if (!strobeSpriteEventOn)
        {
            return;
        }
        timeToNextSprite -= Time.deltaTime;
        if(timeToNextSprite <= 0.0f)
        {
            timeToNextSprite = strobeSpriteEvent.duration;
            int currIndex = strobeSpriteEvent.GetNextIndex();
            if(currIndex == 0)
            {
                backgroundSpriteRenderer.enabled = true;
                for (int i = 0; i < strobeSprRenderers.Count; i++) {
                    strobeSprRenderers[i].enabled = false;
                }
            }
            else
            {
                backgroundSpriteRenderer.enabled = false;
                for (int i = 0; i < strobeSprRenderers.Count; i++)
                {
                    if(currIndex == i+1)
                    {
                        strobeSprRenderers[i].enabled = true;
                    }
                    else
                    {
                        strobeSprRenderers[i].enabled = false;
                    }
                }
            }
        }
    }

    public void StartShakeEvent(ShakeEvent evt)
    {
        if(evt == null)
        {
            return;
        }
        if (evt.duration <= 0.0f)
        {
            shakeRemainingTime = 999999;
        }
        else
        {
            shakeRemainingTime = evt.duration;
        }
        xShakeAmount = evt.xShakeAmount;
        yShakeAmount = evt.yShakeAmount;
        shakeEvent = true;
    }

    public void StartExplodeUI()
    {
        explodeRemainingTime = 999999;
        explodeSpeed = 1.0f;
        explodeUI = true;
        borderFlashAnimator.speed = 1.1f;
        borderFlashAnimator.SetTrigger("flash");
    }

    public void StartExplodeUI(ShakeEvent evt)
    {
        xShakeAmount = evt.xShakeAmount;
        yShakeAmount = evt.yShakeAmount;
        if(evt.duration <= 0.0f)
        {
            explodeRemainingTime = 999999;
        }
        else
        {
            explodeRemainingTime = evt.duration;
        }
        explodeSpeed = 1.0f;
        explodeUI = true;
        borderFlashAnimator.speed = 1.1f;
        borderFlashAnimator.SetTrigger("flash");
    }

    public void IncreaseExplodeUISpeed()
    {
        explodeSpeed += SPEED_INCREASE;
        borderFlashAnimator.speed = borderFlashAnimator.speed + SPEED_INCREASE;
    }

    public void StopExplodeUI()
    {
        explodeUI = false;
        explodeSpeed = 1.0f;
        backgroundSpriteGO.transform.position = new Vector3(0, 0, 0);
        backgroundSpriteGO.transform.localScale = new Vector3(1, 1, 1);
        backgroundSpriteGO.transform.rotation = Quaternion.Euler(0,0,0);
        borderFlashAnimator.SetTrigger("off");
    }

    public void StopShakeEvent()
    {
        shakeEvent = false;
        shakeSpeed = 1.0f;
        backgroundSpriteGO.transform.position = new Vector3(0, 0, 0);
        backgroundSpriteGO.transform.localScale = new Vector3(1, 1, 1);
        backgroundSpriteGO.transform.rotation = Quaternion.Euler(0, 0, 0);
        borderFlashAnimator.ResetTrigger("flash");
    }
}
