﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RubCircleCollision : MonoBehaviour{

    Collider2D myColl;
    public bool pointerIsOver = false;
    float distCheck = 25.0f;

    public static RubCircleCollision instance;

    private void Awake()
    {
        Debug.Log("rcc awake");
        instance = this;
    }

    // Use this for initialization
    void Start () {
        myColl = GetComponent<Collider2D>();

    }
	
	// Update is called once per frame
	void Update () {

        CheckRubbing();
        
	}

    Vector2 lastMousePos;

    private void CheckRubbing()
    {
        if (!pointerIsOver)
        {
            return;
        }
        float dist = Vector2.Distance(lastMousePos, Input.mousePosition);
        Debug.Log("DIST : " + dist);



        if (dist > distCheck)
        {
            RubbingEventSystem.instance.OnRubbed();
            lastMousePos = Input.mousePosition;
        }
    }

    private void OnMouseOver()
    {
        pointerIsOver = true;
    }

    private void OnMouseEnter()
    {
        Debug.Log("Entered");
        pointerIsOver = true;
        lastMousePos = Input.mousePosition;
    }

    private void OnMouseExit()
    {
        Debug.Log("Exited");
        pointerIsOver = false;
    }

}
