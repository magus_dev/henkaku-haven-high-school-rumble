﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugText : MonoBehaviour {

    public static DebugText instance;
    TextMeshProUGUI textMeshProUGUI;

	// Use this for initialization
	void Start () {
        instance = this;
        textMeshProUGUI = GetComponent<TextMeshProUGUI>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void Log(string txtLog)
    {
        textMeshProUGUI.text = txtLog;
    }

}
