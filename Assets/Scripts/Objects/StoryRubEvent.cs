﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class StoryRubEvent : StoryItem {
    public long RUBBED_CHAR_ID;
    public List<RubEventData> rubEventDatas;
    int currentIndex = -1;

    public class RubEventData
    {
        public long ID;
        public float xPos;
        public float yPos;
        public long amount_of_rubs;
        public float time_in_miliseconds;
    }

    public void SetRubData(List<string> rubDatas)
    {
        currentIndex = -1;
        rubEventDatas = new List<RubEventData>();
        for (int i = 0; i < rubDatas.Count; i++)
        {
            string[] rubData = rubDatas[i].Split(new string[] { "\t" }, System.StringSplitOptions.None);
            RubEventData rubEventData = new RubEventData();
            rubEventData.xPos = float.Parse(rubData[0], CultureInfo.InvariantCulture);
            rubEventData.yPos = 544.0f - float.Parse(rubData[1], CultureInfo.InvariantCulture);
            rubEventData.amount_of_rubs = long.Parse(rubData[2], CultureInfo.InvariantCulture);
            rubEventData.time_in_miliseconds = float.Parse(rubData[3], CultureInfo.InvariantCulture);
            rubEventDatas.Add(rubEventData);
            //Debug.Log("Data : " + rubEventData.xPos + " " + rubEventData.yPos + " " + rubEventData.amount_of_rubs + " " + rubEventData.time_in_miliseconds);
        }
    }

    // OLD : 
    public void SetRubData(string data) // for what the data ???
    {
        currentIndex = -1;
        rubEventDatas = new List<RubEventData>();

        TextAsset rub_event_data = Resources.Load<TextAsset>("rubdata/rub_event_" + ID.ToString());
        
        string content = rub_event_data.text;
        if (content == "")
            content = System.Text.Encoding.Default.GetString(rub_event_data.bytes);

        string[] rubLines = content.Split(new string[] { "\r\n", "\n", "\r" }, System.StringSplitOptions.RemoveEmptyEntries);

        // i = 1 cuz first line is header/title of columns
        for (int i = 1; i < rubLines.Length; i++)
        {
            string[] rubData = rubLines[i].Split(new string[] { "\t" }, System.StringSplitOptions.None);
            RubEventData rubEventData = new RubEventData();
            rubEventData.xPos = float.Parse(rubData[0], CultureInfo.InvariantCulture);
            rubEventData.yPos = 544.0f - float.Parse(rubData[1], CultureInfo.InvariantCulture);
            rubEventData.amount_of_rubs = long.Parse(rubData[2], CultureInfo.InvariantCulture);
            rubEventData.time_in_miliseconds = float.Parse(rubData[3], CultureInfo.InvariantCulture);
            rubEventDatas.Add(rubEventData);
            Debug.Log("Data : " + rubEventData.xPos + " " + rubEventData.yPos + " " + rubEventData.amount_of_rubs + " " + rubEventData.time_in_miliseconds);
        }

    }

    public void Reset()
    {
        currentIndex = -1;
    }

    public RubEventData GetNextRubData()
    {
        if (currentIndex < 0)
        {
            currentIndex = 0;
        }
        else
        {
            currentIndex++;
        }

        if (currentIndex >= rubEventDatas.Count)
        {
            return null; // No more data left
        }

        return rubEventDatas[currentIndex];
    }

    public RubEventData GetCurrentRubData()
    {
        if(currentIndex < 0)
        {
            return GetNextRubData();
        }
        if (currentIndex >= rubEventDatas.Count)
        {
            return null; // No more data left
        }
        return rubEventDatas[currentIndex];
    }

}
