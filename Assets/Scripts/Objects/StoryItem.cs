﻿using System.Collections;
using System.Collections.Generic;


public class StoryItem  {
    public enum StoryItemType
    {
        Dialogue,
        RubEvent,
        MoneyDragEvent,
    }
    public string ID;
    public StoryItemType type;
    public string NEXT_ID;
    public string BACKGROUND;
    public List<EffectEvent> additionalEvent;

    public void AddEvent(EffectEvent effectEvent)
    {
        if (additionalEvent == null)
        {
            additionalEvent = new List<EffectEvent>();
        }
        if (effectEvent == null)
        {
            return;
        }
        additionalEvent.Add(effectEvent);
    }

}
