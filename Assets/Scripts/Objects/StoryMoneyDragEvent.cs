﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryMoneyDragEvent : StoryItem
{

    public Vector2 moneyPos;
    public Vector2 destinationPos;
    public float timeToSlide;
    public int direction = 1; // 1 up , -1 down



}
