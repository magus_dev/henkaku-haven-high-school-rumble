﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_PSP2
using UnityEngine.PSVita;
#endif

public class InputSystem : MonoBehaviour  {

    public static InputSystem instance;
    private Touch[] touchesFrontScreen;
    public Touch[] TouchesFrontScreen { get { return touchesFrontScreen; } set { } }
    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);


#if UNITY_PSP2
        PSVitaInput.secondaryTouchIsScreenSpace = true;
#endif

    }



    public bool crossPressedDown = false;

    public bool crossPressed = false;
    public bool circlePressed = false;
    public bool trianglePressed = false;
    public bool squarePressed = false;

    public bool upPressed = false;
    public bool downPressed = false;
    public bool rightPressed = false;
    public bool leftPressed = false;

    public bool leftShoulderPressed = false;
    public bool rightShoulderPressed = false;

    public bool startPressed = false;
    public bool selectPressed = false;

    public float rightStickHorizontal = 0.0f;
    public float rightStickVertical = 0.0f;
    public float leftStickHorizontal = 0.0f;
    public float leftStickVertical = 0.0f;

    public void ReadKeys()
    {
        crossPressedDown = Input.GetButtonDown("Cross");

        crossPressed = Input.GetButton("Cross");
        circlePressed = Input.GetButton("Circle");
        trianglePressed = Input.GetButton("Triangle");
        squarePressed = Input.GetButton("Square");

        upPressed = Input.GetButton("Dup");
        downPressed = Input.GetButton("Ddown");
        rightPressed = Input.GetButton("Dright");
        leftPressed = Input.GetButton("Dleft");

        rightShoulderPressed = Input.GetButton("Right Shoulder");
        leftShoulderPressed = Input.GetButton("Left Shoulder");

        startPressed = Input.GetButton("Start");
        selectPressed = Input.GetButton("Select");

        rightStickHorizontal = Input.GetAxis("Right Stick Horizontal");
        rightStickVertical = Input.GetAxis("Right Stick Vertical");
        leftStickHorizontal = Input.GetAxis("Left Stick Horizontal");
        leftStickVertical = Input.GetAxis("Left Stick Vertical");
    }


    public void ReadTouches()
    {
        touchesFrontScreen = new Touch[Input.touches.Length + 1];
        Array.Copy(Input.touches, touchesFrontScreen, Input.touches.Length);
        touchesFrontScreen[Input.touches.Length] = new Touch();
        touchesFrontScreen[Input.touches.Length].position = Input.mousePosition;
        //string dbgText = "";
        foreach (Touch touch in touchesFrontScreen)
        {
            //dbgText += " pos: " + touch.position.x + ", " + touch.position.y;
            //dbgText += " mp: " + Input.mousePosition.x + ", " + Input.mousePosition.y;
            //dbgText += " fid: " + touch.fingerId;
            //dbgText += " dpos: " + touch.deltaPosition;
            //dbgText += " dtime: " + touch.deltaTime;
            //dbgText += " tapcount: " + touch.tapCount;
            //dbgText += " phase: " + touch.phase;
            //dbgText += "\n";

            if (touch.phase == TouchPhase.Began)
            {
                //print("Began");
            }

            if (touch.phase == TouchPhase.Ended)
            {
                if (touch.tapCount == 2)
                {
                    //print("Ended Multitap");
                }
                else
                {
                    //print("Ended");
                }
            }
        }

        //DebugText.instance.Log(dbgText);

    }

}
