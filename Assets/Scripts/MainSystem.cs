﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PSVita;

public class MainSystem : MonoBehaviour
{
    public static MainSystem instance = null;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {

            //if not, set instance to this
            instance = this;
        }
        //If instance already exists and it's not this:
        else if (instance != this)
        {

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a MainSystem.
            Destroy(gameObject);
        }

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        Application.targetFrameRate = 60;


    }

    // Use this for initialization
    void Start ()
    {
        Application.targetFrameRate = 60;
        CharacterTextSystem.instance.Hide();
        StartCoroutine(StartStory());
    }

    IEnumerator StartStory()
    {
        yield return new WaitForSeconds(0.5f);
        AdvanceToNextStoryItem();
    }
	
	// Update is called once per frame
	void Update () {

        InputSystem.instance.ReadKeys();

        if (InputSystem.instance.startPressed)
        {
            //CharacterTextSystem.instance.DisplayCharacterText("Yifan" , "Hello my name is Ye\r\ne-tan, and this is my world of love and joy and lots of hacking of my girlfriend Vita-chan who is always memleaking on me. But I still love her. Everytime I touch her I'm gonna henkaku !!!");
            //CharacterTextSystem.instance.DisplayCharacterText(StorySystem.instance.GetNextStoryItem());

        } else if (InputSystem.instance.selectPressed)
        {
#if UNITY_PSP2
            //CharacterTextSystem.instance.DisplayCharacterText("System", "cdram : " + UnityEngine.PSVita.Diagnostics.GetFreeMemoryCDRAM() + " lpddr : " + UnityEngine.PSVita.Diagnostics.GetFreeMemoryLPDDR() + " physcont : " + UnityEngine.PSVita.Diagnostics.GetFreeMemoryPHYSCONT());
#endif
            }
        else if (InputSystem.instance.crossPressedDown)
        {
            if (StorySystem.instance.currentItem == null)
            {
                AdvanceToNextStoryItem();
            }
            else if(StorySystem.instance.currentItem.type == StoryItem.StoryItemType.Dialogue)
            {
                if (!CharacterTextSystem.instance.AdvanceText())
                {
                    AdvanceToNextStoryItem();
                    //CharacterTextSystem.instance.Hide();
                }
            }else if(StorySystem.instance.currentItem.type == StoryItem.StoryItemType.RubEvent)
            {
                // ? nothing for now
            }


        }

        if (StorySystem.instance.currentItem != null)
        {
            if (StorySystem.instance.currentItem.type == StoryItem.StoryItemType.RubEvent)
            {

            }
        }


    }

    private void AdvanceToNextStoryItem()
    {
        StoryItem storyItem = StorySystem.instance.GetNextStoryItem();
        if(storyItem == null)
        {
            
            return;
        }
        CharacterDisplaySystem.instance.SetBackground(storyItem.BACKGROUND);
        if (storyItem.type == StoryItem.StoryItemType.Dialogue)
        {
            RubbingEventSystem.instance.Hide();
            CharacterTextSystem.instance.DisplayCharacterText((StoryDialog)storyItem);
        }
        else if (storyItem.type == StoryItem.StoryItemType.RubEvent)
        {
            CharacterTextSystem.instance.Hide();
            StartRubbingEvent((StoryRubEvent)storyItem);
        }
        else if (storyItem.type == StoryItem.StoryItemType.MoneyDragEvent)
        {
            CharacterTextSystem.instance.Hide();
            StartMoneyDraggingEvent((StoryMoneyDragEvent)storyItem);
        }
    }
    private void OnMoneyDraggingEventSuccess()
    {
        AdvanceToNextStoryItem();
    }

    private void OnMoneyDraggingEventFailed()
    {
        StartMoneyDraggingEvent((StoryMoneyDragEvent)StorySystem.instance.currentItem);
    }

    private void StartMoneyDraggingEvent(StoryMoneyDragEvent storyMoneyDragEvent)
    {
        MoneyDraggingEventSystem.instance.StartMoneyDraggingEvent(storyMoneyDragEvent, OnMoneyDraggingEventSuccess, OnMoneyDraggingEventFailed);
    }

    private void OnRubbingEventSuccess()
    {
        AdvanceToNextStoryItem();
    }

    private void OnRubbingEventFailed()
    { 
        StartRubbingEvent((StoryRubEvent)StorySystem.instance.currentItem);
    }

    private void StartRubbingEvent(StoryRubEvent storyRubEvent)
    {
        RubbingEventSystem.instance.StartRubbingEvent(storyRubEvent, OnRubbingEventSuccess, OnRubbingEventFailed);
    }
}
